#include <vector>

class Solution {
public:
	void moveZeroes(std::vector<int>& nums) {
		size_t shift = 0;
		for(size_t i = 0; i < nums.size(); ++i) {
			if (nums[i] == 0) {
				++shift;
			}
			else {
				nums[i-shift] = nums[i];
			}
		}
		for (size_t i = nums.size()-shift; i < nums.size(); ++i) {
			nums[i] = 0;
		}
	}
};