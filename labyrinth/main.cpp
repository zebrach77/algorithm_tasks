#include <iostream>
#include <vector>
#include <string>
using graph = std::vector<std::vector<int> >;
enum {
	white,
	grey,
	black
};
class Labyrinth {
public:
    Labyrinth(size_t n) : n_(n), gr_(n*n, std::vector<int>()) {
        readMatr();
//        std::cout<<"kdnsf\n";
        matrToGraph();
//        std::cout<<"kdnsf\n";
//        print_vector();
//        std::cout<<"kdnsf\n";
		colors_ = *new std::vector<char>(n_*n_);
	    std::cout<<std::endl<<min_leng(player_number_, exit_number_, 0)<<std::endl;
    }

private:
    std::vector<std::vector<char>> matr_;
    graph gr_;
    size_t n_;
    int player_number_;
    int exit_number_;
	std::vector<char> colors_;
    void readMatr() {
        matr_ = *new std::vector<std::vector<char> >(n_, std::vector<char>(n_));
		std::string temp;
        for (size_t i = 0; i < n_; ++i) {
			std::getline(std::cin, temp);
            for (size_t j = 0; j < n_; ++j) {
                matr_[i][j] = temp[j];
            }
        }
    }

    void matrToGraph() {
        for (size_t i = 0; i < n_; ++i) {
            for (size_t j = 0; j <n_; ++j) {
                if (matr_[i][j] == ' ' || matr_[i][j] == '$' || matr_[i][j] == '@') {
                    if (i > 0 && (matr_[i-1][j] == ' ' || matr_[i-1][j] == '@' || matr_[i-1][j] == '$')) {
                        gr_[n_ * i + j].push_back(n_*(i-1)+j);
//                        gr_[n_*(i-1)+j].push_back(n_*i+j);
                    }
                    if (i < n_ - 1 && (matr_[i+1][j] == ' ' || matr_[i+1][j] == '@'|| matr_[i+1][j] == '$')) {
                        gr_[n_ * i + j].push_back(n_*(i+1)+j);
//                        gr_[n_*(i+1)+j].push_back(n_*i+j);
                    }
                    if (j > 0 && (matr_[i][j-1] == ' ' || matr_[i][j-1] == '@' || matr_[i][j-1] == '$')) {
                        gr_[n_ * i + j].push_back(n_*i+j-1);
//                        gr_[n_*i+j-1].push_back(n_*i+j);
                    }
                    if (j < n_ - 1 && (matr_[i][j+1] == ' ' || matr_[i][j+1] == '@' || matr_[i][j+1] == '$')) {
                        gr_[n_ * i + j].push_back(n_*i+j+1);
//                        gr_[n_*i+j+1].push_back(n_*i+j);
                    }
                }
                if (matr_[i][j] == '$') {
                    player_number_ = n_*i+j;
                }
                if (matr_[i][j] == '@') {
                    exit_number_ = n_*i+j;
                }
            }
        }
    }


    void print_vector() {
        for (int i = 0; i < n_*n_; ++i) {
//			auto l = std::unique(gr_[i].begin(), gr_[i].end());
//			gr_[i].erase(l, gr_[i].end());
			std::cout<<i<<"     ";
            for (auto &elem: gr_[i]) {
                std::cout << elem<<" ";
            }
            std::cout << std::endl;
        }
    }

	size_t min_leng(size_t v_curr, size_t v_end, size_t level) {
		if (v_curr == v_end) {
			return level;
		}
		if (colors_[v_curr] == white) {
			size_t minn = gr_.size()+1;
			colors_[v_curr] = grey;
			for (auto &elem:gr_[v_curr]) {
				size_t tm = min_leng(elem, v_end, level+1);
//				std::cout<<"!!!   "<<tm<<std::endl;
				if (tm < minn) {
					minn = tm;
				}
			}
			colors_[v_curr] = black;
			return minn;
		}
		return gr_.size()+1;
	}
};




int main() {
    Labyrinth l(5);
}