#include <iostream>
#include <vector>
#include <limits>

int main() {
	int n;
	std::cin>>n;
	std::vector<int> v(n);
	for(size_t i = 0; i < n; ++i) {
		std::cin>>v[i];
	}
	v.insert(v.begin(), 0);
	std::vector<int> pref_sum;
	std::vector<size_t> indices;
	pref_sum.push_back(v[0]);
	for (size_t i = 1; i < n+1; ++i) {
		pref_sum.push_back(pref_sum[i-1]+v[i]);
	}
	size_t temp_min_ind = 0;
	for (size_t i = 0; i < n+1; ++i) {
		if(pref_sum[i] <= pref_sum[temp_min_ind]) {
			temp_min_ind = i;
		}
		indices.push_back(temp_min_ind);
	}
	int max_sum = std::numeric_limits<int>::min();
	for (size_t r = 0; r < n+1; ++r) {
		int s = pref_sum[r] - pref_sum[indices[r]];
		if (s > max_sum) {
			max_sum = s;
		}
	}

	std::cout<<max_sum<<std::endl;
}
// 1 2 5 -7 -5 78 34   -5
// 1 3 8 1  -4 74 108 103
// r = 6
// 0 0 0 3  4  4  4    4