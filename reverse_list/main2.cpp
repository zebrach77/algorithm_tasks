#include <iostream>

struct ListNode {
	int val;
	ListNode *next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
	ListNode *reverseList(ListNode *head) {
		ListNode* prev = nullptr;
		ListNode* curr = head;
		ListNode* next;
		while (curr) {
			next = curr->next;
			curr->next = prev;
			prev = curr;
			curr = next;
		}
		return prev;
	}
};

int main() {
	Solution w;
	ListNode* l = new ListNode(5);
	l->next = new ListNode(10);
	l->next->next = new ListNode(13);
	l->next->next->next = new ListNode(15);
	l = w.reverseList(l);
	std::cout<<l->val<<" "<<l->next->val<<" "<<l->next->next->val<<" "<<l->next->next->next->val<<" "<<std::endl;
}
