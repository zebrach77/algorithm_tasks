#include <iostream>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
	ListNode* reverseList(ListNode* head) {
		auto a = reverseListHelper(head);
		return a.first;
	}
private:
	std::pair<ListNode*, ListNode*> reverseListHelper(ListNode* head) {
		if (head) {
			if (head->next) {
				auto tempp = reverseListHelper(head->next);
				ListNode* thead = tempp.first;
				ListNode* ttail = tempp.second;
				ttail->next = head;
				head->next = nullptr;
				return {thead, ttail->next};
//				return new ListNode(temp->val, head);
			}
			else {
				return {head, head};
			}
		}
		return {head, head};
	}
};


int main() {
	Solution w;
	ListNode* l = nullptr;
//	l->next = new ListNode(10);
//	l->next->next = new ListNode(13);
//	l->next->next->next = new ListNode(15);
	l = w.reverseList(l);
	std::cout<<l->val<<" "<<l->next->val<<" "<<l->next->next->val<<" "<<l->next->next->next->val<<" "<<std::endl;
}