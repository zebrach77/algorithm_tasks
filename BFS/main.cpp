#include <vector>
#include <queue>
#include <iostream>
enum {
	white,
	grey,
	black
};


void BFS(const std::vector<std::vector<size_t>> &gr, std::vector<char> &colors, size_t v) {
	std::queue<std::pair<size_t, size_t>> q;
	colors[v] = black;
	q.emplace(v, 0);
	size_t curr_level = 0;
	while (!q.empty()) {
		while (!q.empty() && q.front().second == curr_level) {
			auto[from, lvl] = q.front();
			q.pop();
			for (auto &child : gr[from]) {
				if (colors[child] == white) {
					q.emplace(child, curr_level + 1);
					colors[child] = black;
				}
			}
		}
		++curr_level;
	}
}



bool IsConnected(const std::vector<std::vector<size_t>>& gr) {
	std::vector<char> colors(gr.size(), white);
	size_t cnt = 0;
	for (size_t i = 0; i < gr.size(); ++i) {
		if (colors[i] == white) {
			++cnt;
			BFS(gr, colors, i);
		}
	}
	return cnt <= 1;
}

std::vector<std::vector<size_t>> ReadGraph(
		size_t V, size_t E
) {
	std::vector<std::vector<size_t>> gr(V, std::vector<size_t>());
	for (size_t i = 0; i < E; ++i) {
		size_t from, to;
		std::cin >> from >> to;
		--from;
		--to;
		gr[from].push_back(to);
		gr[to].push_back(from);
	}
	return gr;
}


int main() {
	size_t U, V;
	std::cin>>U>>V;
	std::vector<std::vector<size_t>> v = ReadGraph(U, V);
	std::cout<<IsConnected(v)<<std::endl;
}