#include <iostream>
#include <vector>

bool check(std::vector<size_t>& data, size_t l, size_t r) {
  if (r - l < 2) {
    return true;
  }
  if (data[r - 1] <= data[l] + data[l + 1]) {
    return true;
  }
  return false;
}

int main() {
  size_t n;
  std::cin >> n;
  std::vector<size_t> data;
  data.reserve(n);
  for (size_t i = 0; i < n; ++i) {
    size_t x;
    std::cin >> x;
    data.push_back(x);
  }

  std::sort(data.begin(), data.end());
  size_t sum = data[0];
  size_t set_size = 1;
  size_t max_sum = data[0];
  size_t max_set_size = 1;
  size_t l = 0, r = 1;
  while (r < n) {
    if (check(data, l, r + 1)) {
      sum += data[r];
      set_size++;
      ++r;
    } else {
      sum -= data[l];
      set_size--;
      ++l;
    }
    if (sum > max_sum) {
      max_sum = sum;
      max_set_size = set_size;
    }
  }
  std::cout << max_set_size << " " << max_sum;
}
