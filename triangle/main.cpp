#include <iostream>
#include <vector>
#include <algorithm>

bool check(std::vector<int>& v, int l, int r) {
	if (r-l <= 2){
		return true;
	}
	if (v[l]+v[l+1] >= v[r-1]) {
		return true;
	}
	return false;
}


int main() {
	int n;
	std::cin>>n;
	std::vector<int> v(n);
	for (int i = 0; i < n; ++i) {
		std::cin>>v[i];
	}
	std::sort(v.begin(), v.end());
	int left = 0;
	int right = 1;
	int max_len = 1;
	int max_sum = v[0];
	int s = v[0];
	while(right <= v.size()) {
		if (check(v, left, right)) {
			++right;
			s += v[right-1];
		}
		else {
			++left;
			s -= v[left-1];
		}
		std::cout<<s<<"\n";
		if (s > max_sum && check(v, left, right)) {

			max_sum = s;
			max_len = right-left;
		}
	}
	std::cout<<max_len<<" "<<max_sum<<"\n";
}