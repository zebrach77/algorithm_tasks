#include <vector>
#include <iostream>
#include <queue>

class Solution {
public:
    void rotate(std::vector<std::vector<int> >& matrix) {
	}
//private:
    void rotate_helper(std::vector<std::vector<int> >& matrix, int n) {
		std::queue<int> q;
		if (matrix.size() < 2*(n+1)) {
			return;
		}
		for (int i = n; i < matrix.size()-n; ++i) {
			q.push(matrix[n][i]);
		}
		for (int i = n; i < matrix.size()-n; ++i) {
			q.push(matrix[i][matrix.size()-n-1]);
			matrix[i][matrix.size()-n-1] = q.front();
			q.pop();
		}
//		q.push(matrix[matrix.size()-n-1][matrix.size()-n-1]);
		for (int i = n; i < matrix.size()-n; ++i) {
			q.push(matrix[matrix.size() - n - 1][matrix.size() - i - 1]);
			matrix[matrix.size() - n - 1][matrix.size() - i - 1] = q.front();
			q.pop();
		}
//		q.push(matrix[matrix.size() - n - 1][]);
		for (int i = n; i < matrix.size()-n; ++i) {
			matrix[matrix.size() - i - 1][n] = q.front();
			q.pop();
		}
		rotate_helper(matrix, n+1);
	}
};


int main() {
	Solution s;
    std::vector<std::vector<int> > v(4);
	int c = 1;
	for (int i = 0; i < 4; ++i) {
        v[i].__emplace_back(5);
		for (int j = 0; j < 4; ++j) {
			v[i][j] = c;
			++c;
		}
	}
	s.rotate_helper(v, 0);
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			std::cout<<v[i][j]<<" ";
		}
		std::cout<<"\n";
	}
}
