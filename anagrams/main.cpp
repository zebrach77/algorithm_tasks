#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include <algorithm>
class Solution {
public:
	std::vector<std::vector<std::string>> groupAnagrams(std::vector<std::string>& strs) {
		std::unordered_map<std::string, std::vector<std::string>> m;
		for (std::string &elem: strs) {
			std::string s = elem;
			std::sort(s.begin(), s.end());
			m[s].push_back(elem);
		}
		std::vector<std::vector<std::string>> res;
		for (auto&[key, value]: m) {
			res.emplace_back(std::move(value));
		}
		return res;
	}
};


int main() {

}